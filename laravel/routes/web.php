<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login', 'LoginController@authenticate')->name('login');
Route::get('logout', 'LoginController@logout');


Route::middleware('authenticate')->group(function () {

    Route::get('reports','ReportController@getReports');
    Route::get('report/{id}','ReportController@getDetailReport');
    Route::post('report','ReportController@createReport');
    Route::put('report/{id}','ReportController@updateReport');
    Route::delete('report/{id}','ReportController@deleteReport');

    Route::get('times','TimeController@getTimes');
    Route::post('time','TimeController@addTime');
    Route::put('time/{id}','TimeController@editTime');
    Route::delete('time/{id}','TimeController@editTime');

    Route::get('users', 'UserController@getUsers');
}
);
Route::get('notoken/reports','ReportController@getReports');
Route::get('notoken/report/{id}','ReportController@getDetailReport');
Route::post('notoken/report','ReportController@createReport');
Route::put('notoken/report/{id}','ReportController@updateReport');
Route::delete('notoken/report/{id}','ReportController@deleteReport');

Route::get('notoken/times','TimeController@getTimes');
Route::post('notoken/time','TimeController@addTime');
Route::put('notoken/time/{id}','TimeController@editTime');
Route::delete('notoken/time/{id}','TimeController@deleteTime');

Route::get('notoken/users', 'UserController@getUsers');

Route::get('/', function () {
    return response()->json(["msg" => "not found"], 404);
});

?>
