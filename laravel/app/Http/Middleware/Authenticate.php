<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        return route('login');
    }

    // public function handle($request, Closure $next){
    //     $token = $request->header('token');
    //     if ( isset( $_SESSION[$token]) ) $user  = $_SESSION[$token];
    //     else $user = "no token";
    //     if ( $token && $user ) return $next($request);

    //     return  response()->json(['msg' => 'login first', 'token' => $token], 401);
    // }
}
