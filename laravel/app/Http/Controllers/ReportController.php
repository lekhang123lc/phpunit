<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Report;


class ReportController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function getReports(Request $request)
    {
        $filter = $request->only('page', 'from', 'to', 'search', 'uid', 'limit');
        $data   = Report::getReports($filter);

        return response()->json($data);
    }

    public function getDetailReport(Request $request, $id){
        $data   = Report::getDetailReport($id);
        if ( empty($data) ) return response()->json(['msg' => 'not found'], 404);
        return response()->json($data);
    }


    public function createReport(Request $request){
        $data = $request->except( 'create_at', 'modified_at' );
        $now  = Carbon::now('Asia/Ho_Chi_Minh');
        $data['create_at']   = $now->toDateTimeString();
        $data['modified_at'] = $data['create_at'];

        $try  = Report::createReport($data);
        if ( $try ){
            $data['id']  = $try;
            $data['msg'] = 'new report added';
        }
        else return response()->json(['msg' => 'Can not create']);
        $data['create_at']   = $now->format('H:m:s d/m/Y');
        $data['modified_at'] = $data['create_at'];

        return response()->json($data,201);
    }

    public function updateReport(Request $request, $id){
        $data = $request->except( 'create_at', 'modified_at' );
        $now  = Carbon::now('Asia/Ho_Chi_Minh');
        $data['modified_at'] =  $now->toDateTimeString();

        $try  = Report::updateReport($id, $data);
        if ( $try ){
            $data['id']  = $id;
            $data['msg'] = 'report updated';
        }
        else $data['msg'] = 'failed';
        $data['modified_at'] = $now->format('H:m:s d/m/Y');

        return response()->json($data);
    }

    public function deleteReport(Request $request, $id){
        $try  = Report::deleteReport($id);
        if ( $try ) $data['msg'] = 'delete successfullt';
        else $data['msg'] = 'failed';
        $data['id'] = $id;
        return response()->json($data);
    }

}

?>