<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;
//use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $json = [];
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passworded...
            $token = Str::random(50);
            $user = [
                'username'  => $credentials['username']
            ];
            $_SESSION[$token] = $user;
            //session([]);
            $json = [
                'token' => $token,
                'user' => User::getUserByUsername($user['username'])
            ];
        }
        else $json = [ 'msg' => 'failed', 'username' => '' ];

        return response()->json($json);
    }

    public function logout(Request $request){
        $token  = $request->header('token');
        $_SESSION[$token] = null ;
        return response()->json(['msg' => 'ok logout']);
    }
}

?>