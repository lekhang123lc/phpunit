<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function getUsers(Request $request)
    {
        $data = DB::table('user')->select('id', 'name', 'role')->get();
        
        return response()->json($data, 200);
    }
}

?>