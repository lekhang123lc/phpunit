<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    public static function getReports($filter){
        $where = [];
        if ( empty($filter['page'])    ) $filter['page']  = 1;
        if ( empty($filter['limit'])   ) $filter['limit'] = 20;
        if ( !empty($filter['from'])   ) $where[] = [ 'create_at', '>=', $filter['from'] ];
        if ( !empty($filter['to'])     ) $where[] = [ 'create_at', '<=', $filter['to'] ];
        if ( !empty($filter['uid'])    ) $where[] = [ 'report.uid', '=', $filter['uid'] ];
        if ( !empty($filter['search']) ) $where[] = [ 'report',  'like', "%".$filter['search']."%" ];

        $data = DB::table('report')
                    ->selectRaw('report.id, report.uid, report.report, 
                            DATE_FORMAT(create_at, "%H:%i:%s %d/%m/%Y" ) as create_at,
                            DATE_FORMAT(modified_at, "%H:%i:%s %d/%m/%Y" ) as modified_at,
                            user.name')
                    ->leftJoin('user', 'user.id', '=', 'report.uid')
                    ->where($where)->orderByRaw('id DESC')
                    ->paginate($filter['limit']);
        return $data;
    }

    public static function getDetailReport($id){
        $data = DB::table('report')
                    ->selectRaw('report.id, report.uid, report.report, 
                                DATE_FORMAT(create_at, "%H:%i:%s %d/%m/%Y" ) as create_at,
                                DATE_FORMAT(modified_at, "%H:%i:%s %d/%m/%Y" ) as modified_at,
                                user.name')
                    ->leftJoin('user', 'user.id', '=', 'report.uid')
                    ->where( [ ['report.id', '=', $id] ])
                    ->first();
        return $data;
    }

    public static function createReport($data){
        return DB::table('report')->insertGetId( $data );
    }

    public static function updateReport($id, $data){
        $try = DB::table('report')
              ->where([ ['id', '=', $id] ])
              ->update($data);
        return $try;
    }

    public static function deleteReport($id){
        return DB::table('report')->where( [ ['report.id', '=', $id] ])->delete();
    }
}
